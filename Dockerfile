FROM ubuntu:14.04
MAINTAINER Christopher Fedde "chris@fedde.us"

RUN apt-get update 
RUN apt-get upgrade -y
RUN apt-get install -y openssh-server

RUN mkdir /home/sfuser
RUN useradd sfuser
RUN chown sfuser:sfuser  /home/sfuser
RUN usermod -aG sudo sfuser
RUN echo sfuser:changeme | chpasswd -m
RUN sudo -u sfuser ssh-keygen -N '' -f /home/sfuser/.ssh/id_rsa
COPY bin/startup /root/bin/startup

CMD /root/bin/startup
