# Exercises
* I'll describe and do the exercise
* Then I'll stop for you to implement it

# Emulation
# For purposes of Education
* Localhost
* Docker containers

# localhost
## packages
* docker.io
* openssh-server
* ansible
* whois
* fping
* sshpass
* traceroute
* jq
* tree

# Exercise 1

        ls -l ~/.ssh
	ssh-keygen # press return till the prompt comes back
        sudo apt-get install -y git
        git clone https://gitlab.com/cfedde/aghi_ansible.git
        # git clone git@gitlab.com:cfedde/aghi_ansible.git # requires a key
        cd aghi_ansible
	bin/install_packages

        sudo usermod -aG docker $USER

        exit # then log back in
        cd aghi_ansible

# localhost
## configs
* add the hostname to /etc/hosts
* source the environment
* The sfuser
* configuring sudo
* ssh trust sfuser

# Exercise 2

	sudo vi /etc/hosts  # ensure that your hostname is there
	source environment
	mk_sfuser
	sudo update-alternatives --config editor
	sudo visudo # add NOPASSWD: to %sudo
   
# Targets
* Emulating real servers using docker
* Dockerfile
* Default docker IP addresses 
* Make about 5 of them

# Exercise 3

	docker build -t aghi_ansible .  # this might take a while
	for f in 1 2 3 4 5; do docker run -d aghi_ansible; done

	docker ps
	docker ps -q


# Hooking it all up
* sshpass
* Environment variables
* The ```environment``` script
* known_hosts file
* dns
* The ansible "inventory" file
* The ansible config file

# Exercise 4.0

        source environment
        source bin/set_sshpass
	getip
	env | grep ANSIBLE

# Exercise 4.1

	ps -ef | grep dnsmasq # see if you can find the dnsmasq.d directory 
	mk_hosts
	mk_hosts > etc/hosts.cfg

	echo addn-hosts=$AGHI_ANSIBLE_DIR/etc/hosts.cfg |
	sudo tee /etc/dnsmasq.d/ansible_hosts.cfg

	sudo service dnsmasq restart
	docker ps -q | xargs -n1 dig +short  # quality check

# Exercise 4.2

	mk_inventory
	mk_inventory > etc/ansible/hosts

	cp /etc/ansible/ansible.cfg etc/ansible
	vi etc/ansible/ansible.cfg # change remote user to sfuser
        update_known_hosts

# Getting ansible to just work
* first try

# Exercise 5

	ansible class -m ping -k

# Exercise 6

        ansible class -m shell -a uptime
