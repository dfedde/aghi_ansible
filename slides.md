# Who Am I?
* Chris Fedde
* chris@fedde.us
* system programmer
* system administrator
* devops

# Demo one
( real example )

# Methodology
* See One,
* Do One, 
* Teach One.

# Methodology
* Today we get to See One
* and we get to Do One
* The Teach One is for later

# Ground Rules 
* Open forum
* Ask, Interrupt, Comment, Kibitz

# Ground Rules
* Tell me when I'm making a mistake
* you'll get extra credit

# Ground Rules
* Breaks every "couple hours"
* Lunch

# Assumptions
* About the course
* About you

# Assumptions
* You have a reasonable grasp of the linux/unix command line
* pipe
* how to navigate directories
* how to run command lines

# Assumptions
* You have Some Kind of Modern(ish) linux distro with you today.
* Modern enough hardware to run Docker
* Or have access to a remote that is.

# Assumptions
* Pair Admin
* Like Pair Programming
* Solves the cross training problem
* Try It

# Assumptions
* Linux CLI
* [DDG](https://duckduckgo.com/)
* [Ansible Documentation](http://docs.ansible.com/)

# Availability
* This courseware is freely available
* [https://gitlab.com/cfedde/aghi_ansible](https://gitlab.com/cfedde/aghi_ansible)

# License
* Many components have their own copyright and license 
* All software used is free (lebre)
* This course ware is placed in the public domain.

# Exercises
* I'll describe the step
* Then I'll stop for you to implement it

# Any questions before we get into it?

# Manage a complex of servers
* Maybe a large number of servers
* With minimal personel

# Words

# "Complex of Servers"

# "Cluster"
* Overused
* Meaningless

# "Cluster"
* HA Pair
* HPC
* Server Farm
* database servers

# "Server Complex"
* I like it better
* "administrative domain"

# "Server Complex"
* administrative domain?

# "Server Complex"
* kinda like a broadcast domain
* from networking?
* does that help?

# "Server Complex"
* administrative domain?
* Some number of manageable elements
* Supported by a common team of admins
* Shared access policies
* The machines where you change root password when someone leaves

# Managing the server complex

# Managing the server complex
* How did we get here

# Thought Experiment One
* The Heaping Pile of Servers
( story )

# Solving the Problem
* Discipline
* Tools
* Automation
* Contain Complexity
* Build on good solutions

# Emulation

# Emulation
# For purposes of Education
* "Reducto ad absurdum"

# Emulation
# For purposes of Education
* Localhost
* Docker containers

# Capstone Demo
* Run it all
( demo 1)

# About Ansible
* The name
( ddg )

# About Ansible
* Fictional "superluminal" radio
* quantum entanglement
* bla bla bla

# About Ansible
* Orson Scott Card
* Enders Game
* 1977

# About Ansible
* Ursula LeGuin
* Rocannan's World 
* 1966

# About Ansible
* A tool for system configuration management

# About Ansible
## Alternatives
* A bunch of scripts using scp and ssh in a loop
* A slightly smaller set of scripts using pscp and pssh 

# About Ansible
## Alternatives
* cf-engine
* puppet
* rdist
* chef

# About Ansible
## Alternatives
* saltstack
* docker
* Virtualization
* vmware/openstack/EC2/"other"

# About Ansible
* Push vs pull
* ansible-pull
* masterless

#About Ansible
## Requirements
* Manager
* Targets

#About Ansible
## Requirements Manager
* Modern Linux
* Ansible package
* Package management

#About Ansible
## Requirements Targets
* Python 2.3 or better
* sshd
* Some login user
* Privileges if needed.

# Emulated Network 
* localhost  the place where we will run ansible.
* Four target 'servers'. At this level we don't care what their real role is.
* Somewhere down the line we'll want to integrate a new server into our complex

# The Class
## Bootstrap
* localhost
* targets

# Manager
* localhost  the place where we will run ansible.
* Modern Linux
* >= Python 2.3
* sshd 

# Targets
* Four target 

# The Class
## Bootstrap Environment

* localhost
* targets

# localhost

# localhost
## packages
* docker.io
* openssh-server
* ansible
* whois
* fping
* sshpass
* traceroute
* jq
* tree

# localhost
## configs
* add the hostname to /etc/hosts
* source the environment
* The sfuser
* configuring sudo
* ssh trust sfuser

# Targets
* Emulating real servers using docker
* Dockerfile
* Default docker IP addresses 
* Make 5 of them

# Hooking it all up
* sshpass
* Environment variables
* The ```environment``` script
* dns
* (demo)

# The ansible command

* In which we look at command line options
* And run the command against local host
* teaching the sfuser@localhost to trust you

# Capstone Two
* In which you run through the class on your own.

Terms:

* Idempotency

# Notes:

```
sudo update-alternatives --config editor
```
